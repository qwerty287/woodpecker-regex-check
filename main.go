package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"os"
	"path"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"

	"github.com/gobwas/glob"
	"gopkg.in/yaml.v3"
)

type rule struct {
	Regex       string `json:"regex" yaml:"regex"`
	Glob        string `json:"glob" yaml:"glob"`
	Path        string `json:"path" yaml:"path"`
	FilePattern string `json:"pattern" yaml:"pattern"`
	MustContain bool   `json:"must_contain" yaml:"must_contain"`
}

func main() {
	var rules []rule

	if rule, has := ruleFromSettings(); has {
		rules = append(rules, rule)
	}

	if fileRules, err := rulesFromFile(); err != nil {
		log.Fatal(err)
	} else if fileRules != nil {
		rules = append(rules, fileRules...)
	}

	for _, v := range rules {
		var err error
		v, err = v.validate()
		if err != nil {
			log.Fatal(err)
		}
		if err := filepath.Walk(v.Path, func(filepath string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}
			g := glob.MustCompile(v.FilePattern)

			if !info.IsDir() && !strings.HasPrefix(filepath, ".git/") && g.Match(path.Base(filepath)) {
				// check file
				file, err := os.Open(filepath)
				if err != nil {
					return err
				}

				data, err := io.ReadAll(file)
				if err != nil {
					return err
				}

				var res bool
				if v.Regex != "" {
					res, err = regexp.Match(v.Regex, data)
					if err != nil {
						return err
					}
				} else {
					globFile := glob.MustCompile(v.Glob)
					res = globFile.Match(string(data))
				}
				if res && !v.MustContain {
					if v.Regex != "" {
						return fmt.Errorf("found regex %s in file %s", v.Regex, filepath)
					} else {
						return fmt.Errorf("glob %s matches file %s", v.Glob, filepath)
					}
				} else if v.MustContain && !res {
					if v.Regex != "" {
						return fmt.Errorf("could not find regex %s in file %s", v.Regex, filepath)
					} else {
						return fmt.Errorf("glob %s does not match file %s", v.Glob, filepath)
					}
				}
			}

			return nil
		}); err != nil {
			log.Fatal(err)
		}
	}
}

func setting(name string) (string, bool) {
	return os.LookupEnv("PLUGIN_" + strings.ToUpper(name))
}

func ruleFromSettings() (rule, bool) {
	regex, hasRegex := setting("regex")
	globSetting, hasGlob := setting("glob")
	if !hasGlob && !hasRegex {
		return rule{}, false
	}
	pathSetting, _ := setting("path")
	pattern, _ := setting("pattern")
	mustContain := false
	mustContainStr, has := setting("must_contain")
	if has {
		var err error
		mustContain, err = strconv.ParseBool(mustContainStr)
		if err != nil {
			log.Fatal(err)
		}
	}
	return rule{
		Regex:       regex,
		Glob:        globSetting,
		Path:        pathSetting,
		FilePattern: pattern,
		MustContain: mustContain,
	}, true
}

func rulesFromFile() ([]rule, error) {
	var rules []rule
	if file, exist := setting("config"); exist {
		data, err := os.ReadFile(file)
		if err != nil {
			return nil, err
		}

		if filepath.Ext(file) == ".yml" || filepath.Ext(file) == ".yaml" {
			if err := yaml.Unmarshal(data, &rules); err != nil {
				return nil, err
			}
		} else if filepath.Ext(file) == ".json" {
			if err := json.Unmarshal(data, &rules); err != nil {
				return nil, err
			}
		} else {
			return nil, fmt.Errorf("unsupported configuration format")
		}
	}
	return rules, nil
}

func (r rule) validate() (rule, error) {
	if r.Path == "" {
		r.Path = "."
	}
	if r.FilePattern == "" {
		r.FilePattern = "*"
	}
	if r.Regex != "" && r.Glob != "" {
		return rule{}, fmt.Errorf("a rule can only use regex or glob")
	}
	if r.Regex == "" && r.Glob == "" {
		return rule{}, fmt.Errorf("rule needs regex or glob")
	}
	return r, nil
}
