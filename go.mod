module codeberg.org/qwerty287/woodpecker-regex-check

go 1.23

require (
	github.com/gobwas/glob v0.2.3
	gopkg.in/yaml.v3 v3.0.1
)
