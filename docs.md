---
name: Regex Check
author: qwerty287
description: Plugin to check if files contain specified regex or match the wildcard
tags: [regex, wildcard]
containerImage: codeberg.org/qwerty287/woodpecker-regex-check
containerImageUrl: https://codeberg.org/qwerty287/-/packages/container/woodpecker-regex-check/latest
url: https://codeberg.org/qwerty287/woodpecker-regex-check
icon: https://codeberg.org/qwerty287/woodpecker-regex-check/raw/branch/main/regex.svg
---

Plugin to check if files contain specified regex or match the wildcard.

## Usage

Example pipeline:
```yaml
pipeline:
	block-static-imports:
		image: codeberg.org/qwerty287/woodpecker-regex-check
		settings:
			pattern: "*.java"
			regex: "import static .*;"
```

### Config file

With the `config` setting, you can set a path to the config file. This allows you to specify multiple rules. It can be a JSON or a YAML file, must be a list of items with the following options:
 
|Name|Description|Default|
|---|---|---|
|`pattern`|File pattern on which formatting should run| `*` (all files)|
|`path`|Subdirectory of repository root to check|`.` (repository root directory)|
|`regex`|Regex to search for|none|
|`glob`|Wildcard to match file contents|none|
|`must_contain`|If files must (if `true`) or must not (if `false`) contain the regex/wildcard|`false`|

`regex` or `glob` is necessary, but you are not allowed to set both.

## Settings

|Name|Description|Default|
|---|---|---|
|`pattern`|File pattern on which checks should run| `*` (all files)|
|`path`|Subdirectory of repository root to check|`.` (repository root directory)|
|`regex`|Regex to search for|none|
|`glob`|Wildcard to match file contents|none|
|`config`|Path to config file|none|
|`must_contain`|If files must (if `true`) or must not (if `false`) contain the regex/wildcard|`false`|

`regex`, `glob` or `config` is necessary, but you are not allowed to set both `regex` and `glob`.
`pattern` and `glob` use https://github.com/gobwas/glob.
