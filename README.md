# regex-check

Woodpecker plugin to check if files contain specified regex.

For usage information see [the docs](https://codeberg.org/qwerty287/woodpecker-regex-check/src/branch/main/docs.md).

---

Icon source: <https://creazilla.com/nodes/3210593-regex-icon>
