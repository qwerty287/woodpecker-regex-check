ARG GOLANG_VERSION=1.23

FROM --platform=$BUILDPLATFORM golang:${GOLANG_VERSION} AS build

COPY . /src
WORKDIR /src

ARG TARGETOS TARGETARCH
RUN --mount=type=cache,target=/root/.cache/go-build \
    --mount=type=cache,target=/go/pkg \
    go build

FROM alpine:3

COPY --from=build /src/woodpecker-regex-check /bin/woodpecker-regex-check

ENTRYPOINT ["/bin/woodpecker-regex-check"]
